This set dress is supposed to be a maze set in a cargo/shipment yard. 

### How do I get set up? ###
This game is pretty large, the assets folders are the bulk of the memory. 
I made this in Unity and most of the assets I used were from the unity assest's store and one from Google Poly. 

### Asset Packs ###

*RPG/FPS Game Assets for PC/Mobile(Industrial Set v2.0) - Dmitrii Kutsenko
*PBR/FPS Game Assets(Industrial Set v1.0)- Dmitrii Kutsenko
*American Trucks - Shahbaz Awan
*Urban Night Sky - Kendall Weaver
*Low Poly Street Pack - Dynamic Art
*Third Person Controller - Invector
*EasyRoads3D Free v3 - AndaSoft
*Police Car & Helicopter - SICS Games
*Building Construction Crane - Kieran Farr (Google Poly)

